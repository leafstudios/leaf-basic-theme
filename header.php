<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<title><?php wp_title('|', TRUE, 'right'); bloginfo('name'); ?></title>
	<?php wp_head(); ?>
</head>
<body>
	<div id="container">
		<?php if ($leaf_header = get_option('leaf_header')) : ?>
			<div id="header">
				<a rel="bookmark" href="<?php bloginfo('wpurl'); ?>" title="<?php bloginfo('name'); ?>">
					<img src="<?php print $leaf_header['url']; ?>" alt="<?php bloginfo('name'); ?> Header" />
				</a>
			</div>
    <?php endif; ?>
		<?php wp_nav_menu(array('theme_location' => 'main-menu', 'fallback_cb' => FALSE,
			'depth' => 1, 'container_id' => 'main-menu')); ?>