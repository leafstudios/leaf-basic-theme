<div class="wrap">
	<?php if (!empty($message)) : ?>
		<div id="message" class="updated fade">
			<?php print $message; ?>
		</div>
	<?php endif; ?>
	<h2><?php _e('Theme settings'); ?></h2>
	<form action="" method="POST" enctype="multipart/form-data">
		<?php wp_nonce_field('leaf_media_settings'); ?>
		<table class="wp-list-table widefat fixed media" cellspacing="0">
			<thead>
				<tr>
					<th scope="col" class="manage-column column-icon"><?php _e('Bild'); ?></th>
					<th scope="col" class="manage-column column-icon"><?php _e('Titel'); ?></th>
					<th scope="col" class="manage-column column-icon"><?php _e('Välj bild'); ?></th>
					<th scope="col" class="manage-column column-cb check-column"><?php _e('Ta bort'); ?></th>
				</tr>
			</thead>
			<tfoot>
				<tr>
					<th scope="col" class="manage-column column-icon"><?php _e('Bild'); ?></th>
					<th scope="col" class="manage-column column-icon"><?php _e('Titel'); ?></th>
					<th scope="col" class="manage-column column-icon"><?php _e('Välj bild'); ?></th>
					<th scope="col" class="manage-column column-cb check-column"><?php _e('Ta bort'); ?></th>
				</tr>
			</tfoot>
			<tbody id="the-list">
				<?php foreach($images as $key => $image) : ?>
					<tr class="<?php if ($key % 2) : ?>alternate <?php endif; ?>author-self status-inherit" valign="top">
						<td class="column-icon media-icon">
							<?php if (isset($image['data']['url'])) : ?><img src="<?php print $image['data']['url']; ?>" /><?php endif; ?>
						</td>
						<td class="title column-title"><?php print $image['title']; ?></td>
						<td class="parent column-parent">
							<input type="file" name="<?php print $image['id']; ?>" id="<?php print $image['id']; ?>" />
						</td>
						<th scope="row" class="check-column">
							<input type="checkbox" name="<?php print $image['id']; ?>" value="delete" />
						</th>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
		<p>
			<input type="submit" value="<?php _e('Spara'); ?>" name="leaf-upload" id="leaf-upload" class="button-primary" />
		</p>
	</form>
</div>