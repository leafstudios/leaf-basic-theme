<?php
require_once(TEMPLATEPATH . '/class/LeafThemeSettings.php');

if (!defined('LEAF_VERSION')) {
	define('LEAF_VERSION', 'v1.1-8-gb9a2b63');
}

/**
 * Register default stylesheets.
 *
 * Overwrite to replace with your own stylesheets. Also you can just
 * use your own style.css to overwrite css for minor changes.
 */
if (!function_exists('leaf_load_default_stylesheets')) {
	function leaf_load_default_stylesheets() {
		wp_enqueue_style('leaf-stylesheet-reset', get_template_directory_uri() . '/css/reset.css', '', LEAF_VERSION);
		wp_enqueue_style('leaf-stylesheet-wysiwyg', get_template_directory_uri() . '/css/wysiwyg.css', '', LEAF_VERSION);
		wp_enqueue_style('leaf-stylesheet-global', get_template_directory_uri() . '/css/global.css', '', LEAF_VERSION);
		wp_enqueue_style('leaf-stylesheet-style', get_stylesheet_directory_uri() . '/style.css', '', LEAF_VERSION);
	}
}

/**
 * Add styleeshet for admin
 *
 * Overwrite function to add your own stylesheets for admin.
 */
if (!function_exists('leaf_load_admin_stylesheets')) {
	function leaf_load_admin_stylesheets() {
		wp_enqueue_style('leaf-admin-stylesheet', get_template_directory_uri() . '/css/admin.css', '', LEAF_VERSION);
	}
}

/**
 * Add favicon, if exists.
 */
if (!function_exists('leaf_add_favicon')) {
	function leaf_add_favicon() {
		// If no favicon available, exit
		if (!$favicon = get_option('leaf_favicon'))
			return;
		
		print '<link rel="shortcut icon" href="' . $favicon['url'] . '" type="image/vnd.microsoft.icon" />';
	}
}

/**
 * Clear up stuff in wp_head().
 */
if (!function_exists('leaf_clean_head')) {
	function leaf_clean_head() {
		remove_action('wp_head', 'wp_generator');
	}
}

/* Ad actions */
add_action('wp_enqueue_scripts', 'leaf_load_default_stylesheets');
add_action('login_enqueue_scripts', 'leaf_load_admin_stylesheets');
add_action('wp_head', 'leaf_add_favicon', 1);

leaf_clean_head();

/* Register sidebars */
register_sidebar(array('name' => __('Sidebar'), 'id' => 'sidebar',
	'before_title' => '<h2 class="widgettitle">', 'after_title' => '</h2>'));

/* Do theme stuff */
$leafThemeSettings = new LeafThemeSettings();

/* Check if something is supposed to be uploaded. */
if (isset($_POST['leaf-upload']) && wp_verify_nonce($_POST['_wpnonce'], 'leaf_media_settings'))
	$leafThemeSettings->upload($_POST);

/* Create admin page for theme settings. */
add_action('admin_menu', array($leafThemeSettings, 'add_admin_page_to_menu'));

/* Register menus */
register_nav_menu('main-menu', __('Main menu, below header image.'));